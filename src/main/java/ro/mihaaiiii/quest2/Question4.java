package ro.mihaaiiii.quest2;

public class Question4 {
    public static void main(String[] args) {
        System.out.println(yearsTilOneMillion(5000000));



    }


    public static int yearsTilOneMillion(double initialBalance) {
        double year = initialBalance * (5.0 / 100.0);
        double saveMoney = 0;
        int countYear = 0;
        double maxMoney = 1000000.0;
        while (saveMoney < maxMoney) {
            saveMoney += year;
            countYear++;
        }
        return countYear;
    }
}

    /*
    // 5 la suta
    A savings account yields 5% interest annually. This Java function is designed to determine
        how many years it will take for you to have $1,000,000 on deposit given an initial value. The
        parameter represents the initial deposit, and the function should return an integer number
        of years before there will be $1,000,000 or more in the account. Write a loop to determine
        the number of years, and return that value.
        (Hint: Do we know how many times this loop needs to iterate? Does this mean a for loop
        or a while loop is more appropriate?)
        Starting code:
public int yearsTilOneMillion(double initialBalance) {
        return 0;
        } */
