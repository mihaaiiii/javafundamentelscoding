package ro.mihaaiiii.quest2;

public class Question1 {

    /*
            What will be printed by this block of Java code?
            int n = 10;
            while (n < 50) {
                 n = n * 2;
                  }
        System.out.println(n);

           A. 10
           B. 40
           C. 50
           D. 80 // aici

                           **/
    public static void main(String[] args) {
        int n = 10;
        while (n < 50) {
            n = n * 2;
        }
        System.out.println(n);

    }
}
