package ro.mihaaiiii.quest2;

import ro.mihaaiiii.utils.ArrayUtils;

import java.util.Arrays;

public class Question6 {
    public static void main(String[] args) {
        int[] ar = {1, 0, 2, 3, -1, 2};
        System.out.println(findRange(ar));

    }

    public static int findRange(int[] intArray) {
        if (intArray.length == 0) {
            return -1;
        }
        int range = ArrayUtils.sortListMinToMax(intArray) - ArrayUtils.sortListMaxToMin(intArray);
        return range;
    }


}


