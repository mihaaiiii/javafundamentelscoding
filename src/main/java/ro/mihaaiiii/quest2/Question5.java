package ro.mihaaiiii.quest2;

   /* Complete the Java function below to print out all the Strings in the String array parameter
    in reverse order. (The String at the highest index should be printed first, then the String at
    the next highest index, and so on. The String at index 0 should be printed last.)
    For example, if a String array called weekdays had value
    {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday"}
    then this function call:
    printInVerverse(weekdays);
    would print:
    Friday
            Thursday
    Wednesday
            Tuesday
    Monday*/

public class Question5 {
    public static void main(String[] args) {
        String[] ar ={"Monday", "as", "Wednesday", "dd", "Friday"};
        printInVerse(ar);

    }

    public static void printInVerse(String[] stringArrays) {
        for (int i = stringArrays.length; i > 0; i--) {

            System.out.println(stringArrays[i-1]);
        }

    }

}
