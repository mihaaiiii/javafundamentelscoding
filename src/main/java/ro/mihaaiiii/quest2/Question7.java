package ro.mihaaiiii.quest2;

public class Question7 {

    /*  What will be printed by this block of Java code?
        int rows = 3;
          for (int i = 1; i <= rows; i++) {
          String thisRow = "";
          for (int j = 0; j < i; j++) {
              thisRow = thisRow + "#";
          }
          System.out.println(thisRow);
      }*/
    public static void main(String[] args) {
        int rows = 3;
        for (int i = 1; i <= rows; i++) {
            String thisRow = "";
            for (int j = 0; j < i; j++) {
                thisRow = thisRow + "#";
            }
            System.out.println(thisRow);

        }
    }
}