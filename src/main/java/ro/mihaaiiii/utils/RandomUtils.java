package ro.mihaaiiii.utils;

import java.util.Random;

public class RandomUtils {
    private static Random random;

    public static Random getRandom() {
        if (random == null) {
            return random = new Random();
        }
        return random;
    }
}
