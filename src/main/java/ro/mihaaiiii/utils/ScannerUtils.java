package ro.mihaaiiii.utils;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ScannerUtils {
    private static Scanner scanner;

    public static Scanner getScanner() {
        if (scanner == null) {
            scanner = new Scanner(System.in);
        }
        return scanner;
    }

    public static float readFloatFromUser() {
        Scanner keyBoard = getScanner();
        float floatNumber;
        try {
            floatNumber = keyBoard.nextFloat();
        } catch (InputMismatchException exception) {
            System.out.println("Please insert a float number");
            keyBoard.nextLine();
            floatNumber = readFloatFromUser();
        }
        return floatNumber;
    }

    public static double readDoubleFromUser() {
        Scanner keyBoard = getScanner();
        double doubleNumber;
        try {
            doubleNumber = keyBoard.nextDouble();
        } catch (InputMismatchException exception) {
            System.out.println("Please insert a double number");
            keyBoard.nextLine();
            doubleNumber = readFloatFromUser();
        }
        return doubleNumber;
    }

    public static long readLongFromUser() {
        Scanner keyBoard = getScanner();
        long longNumber;
        try {
            longNumber = keyBoard.nextLong();
        } catch (InputMismatchException exception) {
            System.out.println("Please insert a double number");
            keyBoard.nextLine();
            longNumber = readLongFromUser();
        }
        return longNumber;
    }

    public static int readIntFromUser() {
        Scanner keyBoard = getScanner();
        int intNumber;
        try {
            intNumber = keyBoard.nextInt();
        } catch (InputMismatchException exception) {
            System.out.println("Please insert a int number");
            keyBoard.nextLine();
            intNumber = readIntFromUser();
        }
        return intNumber;
    }


}
