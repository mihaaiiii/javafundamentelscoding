package ro.mihaaiiii.utils;

public class ArrayUtils {

    public static int sortListMinToMax(int[] intArray) {
        int largeNumber = 0;
        for (int i = 0; i < intArray.length; i++) {
            for (int j = i + 1; j < intArray.length; j++) {
                if (intArray[i] > intArray[j]) {
                    int js = intArray[i];
                    intArray[i] = intArray[j];
                    intArray[j] = js;
                    largeNumber = js;
                }
            }
        }
        return largeNumber;
    }

    public static int sortListMaxToMin(int[] intArray) {
        int smallNumber = 0;
        for (int i = 0; i < intArray.length; i++) {
            for (int j = i + 1; j < intArray.length; j++) {
                if (intArray[i] < intArray[j]) {
                    int js = intArray[i];
                    intArray[i] = intArray[j];
                    intArray[j] = js;
                    smallNumber = js;

                }
            }
        }
        return smallNumber;
    }
}
