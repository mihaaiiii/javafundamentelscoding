package ro.mihaaiiii.exercices.market;

public class Produs {
    String name;
    double price;

    public Produs(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        final StringBuffer sb =
                new StringBuffer("Produs: ");
        sb.append(name);
        sb.append(" price = ").append(price);
        return sb.toString();
    }
}
