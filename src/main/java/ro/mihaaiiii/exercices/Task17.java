package ro.mihaaiiii.exercices;

import ro.mihaaiiii.utils.ScannerUtils;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class Task17 {
    public static void main(String[] args) {
        System.out.println("Introduceti o data sub forma: dd.mm.yyyy");
        String dateAsString = ScannerUtils.getScanner().nextLine();
        LocalDate nextCourseDate = LocalDate.parse(dateAsString, DateTimeFormatter.ofPattern("dd.MM.yyy"));

        LocalDate currentdate = LocalDate.now();

        long dates = currentdate.until(nextCourseDate, ChronoUnit.DAYS);
        Period days = Period.between(currentdate, nextCourseDate);
        System.out.println(days.getDays());


    }
}
