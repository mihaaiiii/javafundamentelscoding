package ro.mihaaiiii.exercices;

import ro.mihaaiiii.utils.ScannerUtils;


public class TaskPalindrom {

    public static void main(String[] args) {

        String name = ScannerUtils.getScanner().nextLine();
        StringBuilder convertor = new StringBuilder(name);
        String palindoromName = String.valueOf(convertor.reverse());
        if (name.equalsIgnoreCase(palindoromName)) {
            System.out.println("Is Palindrome");
            System.out.println(name + " : " + palindoromName);
        }else {
            System.out.println("is not a Palindrome");
        }
    }

}

