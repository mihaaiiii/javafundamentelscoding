package ro.mihaaiiii.exercices;

import ro.mihaaiiii.utils.ScannerUtils;

public class Task5 {

    public static void main(String[] args) {
        System.out.println("Introduceti un numar pozitiv: ");
        int inputFromUser = ScannerUtils.readIntFromUser();
        for (int i = 2; i < inputFromUser; i++) {
           if(isPrimeNumber(i)){
               System.out.println(i);
           }

        }
    }

    private static boolean isPrimeNumber(int checkNumber) {
        if (checkNumber == 2) {
            return true;
        }
        for (int i = 2; i < checkNumber; i++) {
            if (checkNumber % i == 0) {
                return false;
            }
        }
        return true;
    }
}
