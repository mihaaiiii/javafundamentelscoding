package ro.mihaaiiii.exercices;

import ro.mihaaiiii.utils.ScannerUtils;

public class Task15 {
    public static void main(String[] args) {
        System.out.println("Introduceti 10 numere: ");
        int[] numbers = new int[10];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = ScannerUtils.readIntFromUser();
        }
        Task15 application = new Task15();
        int[] duplicates = application.returnOfDuplicates(numbers);


        for (int number : duplicates) {
            System.out.println(number);

        }

    }

    public static boolean isDuplicateNumberInArray(int number, int[] listOfNumbers) {
        int numberOfOccurancies = 0;
        for (int element : listOfNumbers) {
            if (element == number) {
                numberOfOccurancies++;
            }
        }
        return numberOfOccurancies >= 2;
    }

    public int[] returnOfDuplicates(int[] initialList) {
        int counterDuplicates = 0;
        int temporaryLength = initialList.length;
        int[] duplicatesList = new int[0];
        for (int i = 0; i < temporaryLength; i++) {
            for (int j = i + 1; j < temporaryLength; j++) {
                if (initialList[i] == initialList[j]) {
                    counterDuplicates++;
                    for (int k = j; k < (temporaryLength - 1); k++) {
                        initialList[k] = initialList[k + 1];
                    }
                    temporaryLength--;
                    j--;


                }
            }
            if (counterDuplicates > 0) {
                duplicatesList = addNewElementToList(initialList[i], duplicatesList);
            }
            counterDuplicates = 0;
        }
        return duplicatesList;
    }

    public int[] addNewElementToList(int newElement, int[] oldList) {
        int[] newList = new int[oldList.length + 1];
        for (int i = 0; i < oldList.length; i++) {
            newList[i] = oldList[i];
        }
        newList[newList.length - 1] = newElement;
        return newList;
    }
}