package ro.mihaaiiii.exercices.bank;

public class User {

    private String name;
    private String surName;
    private double money;
    private String cnp;

    public User(String name, String surName, double money, String cnp) {
        this.name = name;
        this.surName = surName;
        this.money = money;
        this.cnp = cnp;
    }

    public User(String name, String surName, String cnp) {
        this.name = name;
        this.surName = surName;
        this.cnp = cnp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("name='").append(name).append('\'');
        sb.append(", surName='").append(surName).append('\'');
        sb.append(", cnp='").append(cnp).append('\'');
        return sb.toString();
    }
}
