package ro.mihaaiiii.exercices.bank;

public class Bank {
    BankAcount bankAcounts[];

    public Bank() {
        //Users
        User mihai = new User("Timis", "Mihai", 70000, "001");
        User denis = new User("Timis", "denis", 70000, "002");
        User vasile = new User("Timis", "vasile", 70000, "003");

        //Acount Users
        BankAcount mihaiAcount = new BankAcount(mihai, 123);
        BankAcount denisAcount = new BankAcount(denis, 450);
        BankAcount vasileAcount = new BankAcount(vasile, 750);

        // List of acount
        bankAcounts = new BankAcount[]{mihaiAcount, denisAcount, vasileAcount};

    }

    // sa permita afisarea tuturor conturilor existente;
    public void showExistingAcount() {
        for (BankAcount bankAcount : bankAcounts) {
            System.out.println("Name: " + bankAcount.getUser().getName() +" "+ bankAcount.getUser().getSurName() + " balance: " + bankAcount.getBalance());
        }
    }

    //afisarea numarului de conturi cu balanta pozitiva / zero;
    public void showAcountWhitPositiveBalance() {
        for (BankAcount bankAcount : bankAcounts) {
            if (bankAcount.getBalance() >= 0) {
                System.out.println(bankAcount.toString());
            }
        }
    }

    //identificare unui cont folosind cnp-ul userului;
    public BankAcount identifyAcount(String cnp) {
        BankAcount acountBank = null;
        for (BankAcount b : bankAcounts) {
            if (cnp.equalsIgnoreCase(b.getUser().getCnp())) {
                acountBank = b;

            }
        }
        return acountBank;
    }

    //sa perminta fiecarui utilizator sa isi acceseze propriul cont bancar din interiorul caruia sa poate face urmatoarele operatiuni:
    //sa aiba posibilitatea sa isi sa vizualizeze balanta contului;
    public void showBalanceAcount(String cnp) {
        System.out.println(identifyAcount(cnp).getBalance());

    }

    //sa depuna bani in cont;
    public void addMoneyToBankAcount(String cnp, double money) {
        User user = identifyAcount(cnp).getUser();
        BankAcount bankAcount = identifyAcount(cnp);
        double moneyUser = user.getMoney();
        if (money > moneyUser) {
            System.out.println("You don't have money!");
            System.out.println("You have: " + moneyUser + " money");
        } else if (money <= 0) {
            System.out.println("Can't inster negative numbers");
        } else {
            bankAcount.setBalance(bankAcount.getBalance() + money);
            user.setMoney(moneyUser - money);
            System.out.println("You have: " + user.getMoney() + " money");
        }
    }

    //sa scoata bani din cont (doar daca are suficienti bani in cont);
    public void addMoneyToUser(String cnp, double money) {
        User user = identifyAcount(cnp).getUser();
        BankAcount bankAcount = identifyAcount(cnp);
        double moneyUser = user.getMoney();
        double acountMoney = bankAcount.getBalance();
        if (acountMoney <= 0 || money > acountMoney) {
            System.out.println("You don't have money!");
        } else {
            // Get Money from acount and decrees the value
            bankAcount.setBalance(acountMoney - money);
            //Add money to user acount
            user.setMoney(moneyUser + money);
            System.out.println(user.getMoney());
            System.out.println(bankAcount.getBalance());
        }


    }

    //sa transfere bani catre un alt utilizator (doar daca are suficienti bani in cont);
    public void transferMoney(String cnpTransfer, Double money) {
        User us = identifyAcount(cnpTransfer).getUser();
        us.setMoney(us.getMoney() + money);
        System.out.println(us.getMoney());
    }

    //	Hint: Fiecare cont bancar va avea un user.


}