package ro.mihaaiiii.exercices.bank;

import ro.mihaaiiii.utils.ScannerUtils;

public class Main {
    public static void main(String[] args) {
        Bank transilvania = new Bank();
        String yesAnwser = "";

        do {

            System.out.println("Alegeti urmatoarele optiuni: ");
            yesAnwser = ScannerUtils.getScanner().nextLine();
            switch (yesAnwser) {
                case "ShowBalance":
                    System.out.println("aici");
                    transilvania.showExistingAcount();
                    break;
                case "ShowPositiveBalance":
                    transilvania.showAcountWhitPositiveBalance();
                    break;
                case "CheckAcountWhitCnp":
                    System.out.println("Introduceti cnp-ul");
                    String verify = ScannerUtils.getScanner().nextLine();
                    System.out.println(transilvania.identifyAcount(verify));
                    break;
                case "Transfer":
                    System.out.println("Introduceti cnp-ul");
                    String cnp = ScannerUtils.getScanner().nextLine();
                    System.out.println("Introduceti suma pe care doriti sa o transferati:");
                    double sum = ScannerUtils.readDoubleFromUser();
                    ScannerUtils.getScanner().nextLine();
                    transilvania.addMoneyToUser(cnp, sum);
                    break;
                default:
                    System.out.println("Nu ait ales nici una din optiunile de mai sus!");

            }
        } while (!yesAnwser.equalsIgnoreCase("Leave"));



    /*
    Sa se simuleze o banca (Bank) care va avea multe conturi bancare (BankAccount) si urmatoarele functionalitati:

    sa permita afisarea tuturor conturilor existente;
    afisarea numarului de conturi cu balanta pozitiva / zero;
    identificare unui cont folosind cnp-ul userului;
    sa perminta fiecarui utilizator sa isi acceseze propriul cont bancar din interiorul caruia sa poate face urmatoarele operatiuni:
    sa aiba posibilitatea sa isi sa vizualizeze balanta contului;
    sa depuna bani in cont;
    sa scoata bani din cont (doar daca are suficienti bani in cont);
    sa transfere bani catre un alt utilizator (doar daca are suficienti bani in cont);
    Hint: Fiecare cont bancar va avea un user.
    */
    }
}

