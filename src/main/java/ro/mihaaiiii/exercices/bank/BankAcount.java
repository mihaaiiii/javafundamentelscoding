package ro.mihaaiiii.exercices.bank;

public class BankAcount {
    private User user;
    private double balance;

    public BankAcount(User user, double balance) {
        this.user = user;
        this.balance = balance;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("BankAcount{");
        sb.append("").append(user);
        sb.append(", balance=").append(balance);
        sb.append('}');
        return sb.toString();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
}
