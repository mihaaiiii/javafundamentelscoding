package ro.mihaaiiii.exercices;

import ro.mihaaiiii.utils.ScannerUtils;

import java.util.Random;

public class Task20 {
    public static void main(String[] args) {
        Task20 game = new Task20();
        game.play();

    }

    public void play() {
        int counter = 0;
        Random randomNumber = new Random();
        int generator = 1 + randomNumber.nextInt(100);
        int number;
        System.out.println("ghiceste numarul:");
        System.out.println(generator);
        do {
            counter ++;
            number = ScannerUtils.readIntFromUser();
            if (number > generator) {
                System.out.println("Too large");

            } else if (number < generator) {
                System.out.println("Too little");
            } else {
                System.out.println("Congratiulation!" + counter);
            }


        } while (number != generator);


    }
}
