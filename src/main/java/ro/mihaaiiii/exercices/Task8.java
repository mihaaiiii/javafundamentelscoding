package ro.mihaaiiii.exercices;

import ro.mihaaiiii.utils.ScannerUtils;

public class Task8 {

    public static void main(String[] args) {
        System.out.println("Enter first number");
        float firtNumber = ScannerUtils.readFloatFromUser();
        System.out.println("Enter operation +, -, /, *: ");
        ScannerUtils.getScanner().nextLine();
        char simbolOperation = ScannerUtils.getScanner().nextLine().charAt(0);
        System.out.println("enter second number;");
        float secondNumber = ScannerUtils.readFloatFromUser();
        float result = 0;
        switch (simbolOperation) {
            case '-':
                result = firtNumber - secondNumber;
                break;
            case '+':
                result = firtNumber + secondNumber;
                break;
            case '*':
                result = firtNumber * secondNumber;
                break;
            case '/':
                if (secondNumber == 0) {
                    System.out.println("Splitting at 0 is not possible");

                } else {
                    result = firtNumber / secondNumber;
                }
                break;
            default:
                System.out.println("it is inconsistent with the principles of mathematics");
        }
        System.out.println(result);
    }

}
