package ro.mihaaiiii.exercices;

import ro.mihaaiiii.utils.ScannerUtils;

public class Task16 {
    public static void main(String[] args) {
        System.out.println("Introduceti 10 numere");
        int listOfNumbers[] = new int[10];
        for (int i = 0; i < listOfNumbers.length; i++) {
            listOfNumbers[i] = ScannerUtils.readIntFromUser();
        }
        int longestSubsequence = 1;
        int temporarySubsequence = 1;


        for (int i = 1; i < listOfNumbers.length; i++) {
            if (listOfNumbers[i] > listOfNumbers[i - 1]) {
                temporarySubsequence++;

            } else {
                if (temporarySubsequence > longestSubsequence) {
                    longestSubsequence = temporarySubsequence;
                }
                temporarySubsequence = 1;

            }
        }
        if (temporarySubsequence > longestSubsequence) {
            longestSubsequence = temporarySubsequence;
        }
        System.out.println(longestSubsequence);

    }
}
