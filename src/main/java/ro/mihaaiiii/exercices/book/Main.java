package ro.mihaaiiii.exercices.book;

import java.util.Arrays;

public class Main {


    public static void main(String[] args) {
        Poem[] poems = new Poem[]{
                new Poem(200, new Author("Mihais", "roman")),
                new Poem(160, new Author("Mihaia", "roman")),
                new Poem(300, new Author("Mihaif", "roman")),
                new Poem(15, new Author("Mihai", "roman"))};

        System.out.println(calculateLongesPoem(poems).getCreator().getSurName());
    }

    public static Poem calculateLongesPoem(Poem[] listOfPoems) {
        Poem longesPoem = listOfPoems[0];
        for (int i = 1; i < listOfPoems.length; i++) {
            if (listOfPoems[i].getStropheNumbers() > listOfPoems[i - 1].getStropheNumbers()) {
                longesPoem = listOfPoems[i];
            }

        }
        return longesPoem;
    }
}



