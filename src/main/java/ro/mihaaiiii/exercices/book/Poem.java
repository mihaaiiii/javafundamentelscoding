package ro.mihaaiiii.exercices.book;

public class Poem {
    private int stropheNumbers;
    private Author creator;


    public Poem(int stropheNumbers, Author creator) {
        this.stropheNumbers = stropheNumbers;
        this.creator = creator;
    }

    public int getStropheNumbers() {
        return stropheNumbers;
    }

    public void setStropheNumbers(int stropheNumbers) {
        this.stropheNumbers = stropheNumbers;
    }

    public Author getCreator() {
        return creator;
    }

    public void setCreator(Author creator) {
        this.creator = creator;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Poem{");
        sb.append("stropheNumbers=").append(stropheNumbers);
        sb.append(", creator=").append(creator);
        sb.append('}');
        return sb.toString();
    }
}
