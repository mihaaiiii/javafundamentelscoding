package ro.mihaaiiii.exercices.book;

public class Author {

   private String surName;
   private String nationality;

    public Author(String surName, String nationality) {
        this.surName = surName;
        this.nationality = nationality;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }
}
