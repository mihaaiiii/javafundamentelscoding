package ro.mihaaiiii.exercices;

import ro.mihaaiiii.utils.ScannerUtils;

public class Task9 {
    public static void main(String[] args) {
        System.out.println("Enter number of wave: ");
        int numberOfWaves = ScannerUtils.readIntFromUser();
        System.out.println("Enter number of line: ");
        int numberOfL = ScannerUtils.readIntFromUser();
        String star = "*";
        String space = "";

        for (int i = 1; i <= numberOfL; i++) {
            int nrOfSpaceBStar = (2 * numberOfL) - (2 * i);
            String ls = "%" + i + "s%" + nrOfSpaceBStar + "s%-" + i + "s";
            if (nrOfSpaceBStar == 0) {
                ls = "%" + i + "s%s%-" + i + "s";
            }
            for (int k = 0; k < numberOfWaves; k++) {
                System.out.printf(ls, star, space, star);
            }

            System.out.println();
        }


    }
}
