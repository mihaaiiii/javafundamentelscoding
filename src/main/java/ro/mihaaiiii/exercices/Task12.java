package ro.mihaaiiii.exercices;

import ro.mihaaiiii.utils.ScannerUtils;

public class Task12 {

    public static void main(String[] args) {
        System.out.println("Enter a text!");
        String text = ScannerUtils.getScanner().nextLine();
        char litera = ' ';
        double procent = procentCaracter(text, litera);
        System.out.printf(litera + "%c, reprezinta %f din numarul total de litere!",litera, procent);

        //    System.out.println(12.0 * (50.0/ 100.0));
    }

    public static double procentCaracter(String text, char character) {
        double count = 0;
        char[] lit = text.toCharArray();
        for (int i = 1; i < lit.length; i++) {
            if (lit[i] == character) {
                count += 1;
            }
        }
        return (count * 100.0) / (double) text.length();
    }

}
