package ro.mihaaiiii.exercices.company;

import ro.mihaaiiii.exercices.book.Poem;
import ro.mihaaiiii.utils.ScannerUtils;

import java.time.LocalDate;
import java.time.Period;

public class Company {
    Employ[] emp;
    private int age = 2;

    public Company() {
        emp = new Employ[0];
    }


    public void addNewEmployToList(Employ newElement) {
        Employ[] newList = new Employ[emp.length + 1];
        for (int i = 0; i < emp.length; i++) {

            newList[i] = emp[i];
        }
        newList[newList.length - 1] = newElement;
        emp = newList;
    }

    public int getNumberOfEmployWhitExperience(int numberOfAge) {
        int noumberOfEmploy = 0;
        for (Employ employ : emp) {
            if (Period.between(employ.getDateOfEmployment(), LocalDate.now()).getYears() > numberOfAge) {

                noumberOfEmploy++;
            }
        }
        return noumberOfEmploy;
    }

    public boolean isMailValid(String email) {
        return email.matches("^(.+)@(\\S+)$");
    }

    public Employ showYungerEmploye(Employ[] employs) {
        Employ employ = employs[0];
        for (int i = 0; i < employs.length - 1; i++) {
            int firstYear = Period.between(employs[i].getDateOfEmployment(), LocalDate.now()).getDays();
            int secondYear = Period.between(employs[i + 1].getDateOfEmployment(), LocalDate.now()).getDays();
            if (firstYear > secondYear) {
                employ = employs[i ];
            }
        }
        return employ;

    }


    public int getAge() {
        return age;
    }


}



