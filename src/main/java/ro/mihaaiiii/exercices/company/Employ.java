package ro.mihaaiiii.exercices.company;

import ro.mihaaiiii.utils.ScannerUtils;

import java.time.LocalDate;

public class Employ {
    private String name;
    private String surName;
    private LocalDate dateOfEmployment;

    private String email;

    public Employ(String name, String surName, LocalDate dateOfEmployment) {
        this.name = name;
        this.surName = surName;
        this.dateOfEmployment = dateOfEmployment;
    }

    public Employ(String name, String surName, LocalDate dateOfEmployment, String email) {
        this.name = name;
        this.surName = surName;
        this.dateOfEmployment = dateOfEmployment;
        this.email = email;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public LocalDate getDateOfEmployment() {
        return dateOfEmployment;
    }

    public void setDateOfEmployment(LocalDate dateOfEmployment) {
        this.dateOfEmployment = dateOfEmployment;
    }

    public String getEmail() {
        return email;
    }

    private void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Employ{");
        sb.append("name='").append(name).append('\'');
        sb.append(", surName='").append(surName).append('\'');
        sb.append(", dateOfEmployment=").append(dateOfEmployment);
        sb.append(", email='").append(email).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
