package ro.mihaaiiii.quests;

public class Question2 {

    /*
    What value for register will be printed at the end of this block of Java code?
        A. 19.0
        B. 19.5
        C. 22.5
        D. 25.5
    */
    public static void main(String[] args) {
        double register = 10.0;
        register = register + 5;
        register = register - 2.5;
        register = register + 10;
        register = register - 3;
        System.out.println("The answer is: " + register);
    }
}
