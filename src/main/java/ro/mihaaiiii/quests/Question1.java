package ro.mihaaiiii.quests;

public class Question1 {
    /*
    Which of the following Java variable declarations has an error?
        A. int x = 5;
        B. double temperature = 75.6;
        C. char grade = ’A’;
        D. String name = ’Adam’;
        */
    public static void main(String[] args) {


        int x = 5;
        double temperature = 75.6;
        char grade = 'A';
        System.out.println(" " + x + temperature + grade);
        //  String name = 'Adam';// Here is the error

    }
}
