package ro.mihaaiiii;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
//        int numar = 2;
//
//        switch (numar) {
//            case 1, 3, 5, 7, 9 -> System.out.println("I am the odd positive number");
//            case 2, 4, 6, 8 -> System.out.println("I am an even positive number");
//            default -> System.out.println("I'm not a positive odd number");  sasa
//        }
        int numar2 = 1;

        switch (numar2) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 9:
                System.out.println("numar impar");
                break;
            case 2:
            case 4:
            case 6:
            case 8:
                System.out.println("numar par");
                break;
            default:
                System.out.println("Nu e numar");
        }
    }
}