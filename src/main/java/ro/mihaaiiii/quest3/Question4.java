package ro.mihaaiiii.quest3;

public class Question4 {
    /* Write a Java function called absoluteValue(). The access modifier should be public, it
     should have a return type of double, and it should take one double parameter as input. If
     the double parameter is less than 0, it should return that number negated. Otherwise, it
     should return the parameter unchanged.
 */
    public static void main(String[] args) {
        double param = -2.215;
        if (param < 0) {
            System.out.println((int) absoluteValue(param));
        }else {
            System.out.println(absoluteValue(param));
        }
    }


    public static double absoluteValue(double input) {
        return input;
    }

}