package ro.mihaaiiii.quest3;
/*

        Define two functions. The first should be called fahrenheitToCelsius(). It should be
          a public function with return type double that takes a double argument that represents a
        temperature in Fahrenheit degrees. It should return the equivalent temperature in Celsius
        degrees. (To convert from Fahrenheit to Celsius, use the formula C = (F − 32) × 5/9.)
        Next, define a function called printTemperature(). It should be public, it should have a
        return type of void, and it should take a double parameter that represents a temperature in
        Fahrenheit degrees. This function should print “F: ” followed by the Fahrenheit parameter,
        then “C: ” followed by the equivalent value in Celsius degrees. Use the first function you
        defined to calculate the appropriate Celsius value inside the second function.
        Bonus challenge: write javadoc comments for both functions.

*/


import ro.mihaaiiii.utils.RandomUtils;

public class Question7 {
    public static void main(String[] args) {

        printTemperature(51);

    }


    /**
     * Convert Fahrenheit to Celsius
     *
     * @param temperature double tipe Fahrenheit degrees
     * @return double value Celsius degrees
     */
    public static double fahrenheitToCelsius(double temperature) {
        return (temperature - 32.0) * (5.0 / 9.0);
    }

    /**
     * Print degrees Celsius and Fahrenheit
     *
     * @param degrees double value represented by the Fahrenheit degrees
     */
    public static void printTemperature(double degrees) {
        double in = fahrenheitToCelsius(degrees);
        System.out.printf("F: %.2f", degrees);
        System.out.println();
        System.out.printf("C: %.2f", in);
    }

}
