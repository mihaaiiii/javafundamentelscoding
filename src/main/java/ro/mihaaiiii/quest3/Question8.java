package ro.mihaaiiii.quest3;
/*

        Define a function called monopolyRoll(). This function provides a random result based on
        the dice-rolling rules for the board game Monopoly. In Monopoly, players roll two six-sided
        dice to determine their move. If they roll the same value on both dice, this is called “rolling
        doubles,” and it means they go again. In our simplified version, the dice-roll function should
        behave like this:

        1. Generate two random integers in the 1 to 6 range.
        2. If the numbers are not the same, return the sum.
        3. If the numbers are the same, generate two more random integers in the 1 to 6 range,
          and return the sum of all 4 numbers.

        Hint: to make your code neater, you can define a second function that generates a random
        integer in the 1 to 6 range (or in the 1 to x range based on a parameter) so that you do not
        need to keep repeating that code.

*/


import ro.mihaaiiii.utils.RandomUtils;

public class Question8 {
    public static void main(String[] args) {
        System.out.println("Sum is " + momopolyRoll());


    }

    public static int momopolyRoll() {
        //  1. Generate two random integers in the 1 to 6 range.
        int cZar1 = RandomUtils.getRandom().nextInt(6) + 1;
        System.out.println("Czar1 = " + cZar1);
        int cZar2 = RandomUtils.getRandom().nextInt(6) + 1;
        System.out.println("Czar2 = " + cZar2);
        int sumCzar = cZar1 + cZar2;
        //    2. If the numbers are not the same, return the sum.
        if (cZar1 != cZar2) {
            return sumCzar;
        } else {
            cZar1 = RandomUtils.getRandom().nextInt(6) + 1;
            System.out.println("DoubleCzar1 = " + cZar1);
            cZar2 = RandomUtils.getRandom().nextInt(6) + 1;
            System.out.println("doubleCzar2 = " + cZar2);
            sumCzar += cZar1 + cZar2;
            System.out.println("DoubleSum = " + sumCzar);

            return sumCzar;

        }

    }


}
