package ro.mihaaiiii.quest3;

/*

        Write a Java function named calculateTip(). The access modifier should be public,
        it should have a return type of double, and it should take as input a double parameter
        representing the cost of a meal at a restaurant. And finally, it should return a double equal
        to 15% of the cost parameter.

*/

public class Question5 {
    public static void main(String[] args) {
        System.out.println(calculateTip(700));
    }

    public static double calculateTip(double cost) {
        return (15 * cost) / 100;
    }
}
