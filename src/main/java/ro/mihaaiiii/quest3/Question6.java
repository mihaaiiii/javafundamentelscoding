package ro.mihaaiiii.quest3;


/*

        Write a Java function called nametagText(). The access modifier should be public, the
        return type should be String, and it should take a String parameter called name. In the
        body of the function, return the String “Hello, my name is ” with the name parameter added
        to the end. (Hint: use String concatenation.)

*/


public class Question6 {
    public static void main(String[] args) {
        System.out.println(nameTagText("Mihai"));
    }

    public static String nameTagText(String name) {

        return "Hello, my name is ".concat(name);
    }
}
